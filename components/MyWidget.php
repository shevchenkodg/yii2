<?php
namespace app\components;
use yii\base\Widget;


class MyWidget extends Widget {

	public $name;

	public function init() {
		parent::init();

//		if($this->name === null) {
//			$this->name = 'Гость';
//		}

		// Буферизируем вывод (что указано между widget::begin и widget::end)
		ob_start();
	}

	public function run() {
		// Помещаем буферизированный вывод
		$content = ob_get_clean();
		// Переводим содержимое в верхний регистр
		$content = mb_strtoupper($content);

//		return $this->render('my', ['name' => $this->name]);
		return $this->render('my', compact('content'));
	}
}