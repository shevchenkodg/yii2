<?php

namespace app\controllers;

use Yii;
use app\models\TestForm;
use app\models\Category;


class PostController extends AppController {

	public $layout = 'basic';

//	public function beforeAction($action) {
//		// Отключение csrf валидации
//		if($action->id == 'index') {
//			$this->enableCsrfValidation = false;
//		}
//
//		return parent::beforeAction($action);
//	}

	public function actionIndex() {

		if(Yii::$app->request->isAjax) {

//			debug($_POST);

			debug(Yii::$app->request->post());

			return 'test';
		}


		/*
		 * Обновление записи в БД
		 */
//		$post = TestForm::findOne(3);
//		$post->email = '3@2.com';
//		$post->save();


		/*
		 * Удаление записи из БД
		 */
//		$post = TestForm::findOne(2);
//		$post->delete();
		// Удаление записей, в которых id > 3
		TestForm::deleteAll(['>', 'id', 3]);


		/*
		 * Запись записи в БД
		 */
		$model = new TestForm();

//		$model->name = 'Автор';
//		$model->email = 'mail@mail.com';
//		$model->text = 'Текст сообщения';
//		$model->save();

		// Если данные загружены
		if($model->load(Yii::$app->request->post())) {
//			debug(Yii::$app->request->post());
			if($model->save()) {
				Yii::$app->session->setFlash('success', 'Данные приняты');
				// Очистка формы после отправки
				return $this->refresh();
			}
			else {
				Yii::$app->session->setFlash('error', 'Ошибка');
			}
		}

		$this->view->title = 'Все статьи';

		return $this->render('test', compact('model'));
	}

	public function actionTest() {

		return $this->render('test');
	}

	public function actionShow() {

//		$this->layout = 'basic';

		$this->view->title = 'Одна статья';
		$this->view->registerMetaTag(['name' => 'keywords', 'content' => 'ключевые слова']);
		$this->view->registerMetaTag(['name' => 'description', 'content' => 'описание страницы']);

//		$cats = Category::find()->all();
//		$cats = Category::find()->orderBy(['id' => SORT_DESC])->all();
//		$cats = Category::find()->asArray()->where('parent=691')->all();
//		$cats = Category::find()->asArray()->where(['parent' => 691])->all();
		// %-маркеры фреймворк ставит самостоятельно
//		$cats = Category::find()->asArray()->where(['like', 'title', 'pp'])->all();
//		$cats = Category::find()->asArray()->where(['<=', 'id', 695])->all();
//		$cats = Category::find()->asArray()->where('parent=691')->limit(2)->all();
		// Избыточный запрос на выборку одной записи
//		$cats = Category::find()->asArray()->where('parent=691')->one();
		// Неизбыточный запрос на выборку одной записи
//		$cats = Category::find()->asArray()->where('parent=691')->limit(1)->one();
//		$cats = Category::find()->asArray()->where('parent=691')->count();
//		$cats = Category::findOne(['parent' => 691]);
//		$cats = Category::findAll(['parent' => 691]);

//		$query = "SELECT * FROM categories WHERE title LIKE '%pp%'";
		// Безопасный аналог запроса для избежания sql-иньекций
//		$query = "SELECT * FROM categories WHERE title LIKE :search";
//		$cats = Category::findBySql($query, [':search' => '%pp%'])->all();

//		$cats = Category::findOne(694);
//		$cats = Category::find()->with('products')->where('id=694')->all();
		// Отложенная загрузка
//		$cats = Category::find()->all();
		// Жадная загрузка
		$cats = Category::find()->with('products')->all();

		return $this->render('show', compact('cats'));
	}

}